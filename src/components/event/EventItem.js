import React from 'react'
import { Card } from 'antd'
import styled from 'styled-components'

const EventWrapper = styled.div`
  transition-duration: 0.3s;
  cursor: pointer;

  :hover {
    box-shadow: 5px 5px 5px 0px #00000042;
    transform: translateY(-2px);
  }

  p {
    color: #000000d1;

    span {
      color: gray;
    }
  }
`
function EventItem(props) {
  const { item } = props
  const { title, description, location, time, date } = item

  return (
    <EventWrapper>
      <Card title={title} style={{ width: '100%' }}>
        <p>
          Description: <span>{description}</span>
        </p>
        <p>
          Location: <span>{location}</span>
        </p>
        <p>
          Time: <span>{time}</span>
        </p>
        <p>
          Date: <span>{date}</span>
        </p>
      </Card>
    </EventWrapper>
  )
}

export default EventItem
