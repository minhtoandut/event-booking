import React, { useState } from 'react'
import { Button, Form, Input, Modal } from 'antd'

function FormSignin(props) {
  const { form, firebase } = props
  const { getFieldDecorator, getFieldsValue } = form

  const [visible, setVisible] = useState(false)

  const userLogin = ({ email, password }) => {
    return firebase.login({ email, password })
  }

  async function handleLogin() {
    const data = getFieldsValue()
    try {
      await userLogin(data)
      setVisible(false)
      form.resetFields()
    } catch (err) {
      console.log(err)
    }
  }

  function handleCancel() {
    setVisible(false)
    form.resetFields()
  }

  return (
    <div style={{ float: 'right' }}>
      <Button onClick={() => setVisible(true)}>Signin</Button>
      <Modal
        visible={visible}
        onOk={handleLogin}
        onCancel={handleCancel}
        okText="Login">
        <Form layout="vertical">
          <Form.Item label="Email">
            {getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: 'Please input the email!',
                },
              ],
            })(<Input />)}
          </Form.Item>

          <Form.Item label="Password">
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Please input the password!',
                },
              ],
            })(<Input type="password" />)}
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default Form.create()(FormSignin)
