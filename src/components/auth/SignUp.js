import React, { useState } from 'react'
import { Button, Form, Input, Modal } from 'antd'

function FormSingup(props) {
  const { form, firebase } = props
  const { getFieldDecorator, getFieldsValue } = form

  const [visible, setVisible] = useState(false)

  const createNewUser = ({ email, password, username }) => {
    return firebase.createUser({ email, password }, { username, email })
  }

  async function handleCreate() {
    const data = getFieldsValue()
    try {
      await createNewUser(data)
      setVisible(false)
      form.resetFields()
    } catch (err) {
      console.log(err)
    }
  }

  function handleCancel() {
    setVisible(false)
    form.resetFields()
  }

  return (
    <div style={{ float: 'right' }}>
      <Button onClick={() => setVisible(true)}>Sign up</Button>
      <Modal
        visible={visible}
        onOk={handleCreate}
        onCancel={handleCancel}
        okText="Create">
        <Form layout="vertical">
          <Form.Item label="Username">
            {getFieldDecorator('username', {
              rules: [
                {
                  required: true,
                  message: 'Please input the username!',
                },
              ],
            })(<Input />)}
          </Form.Item>

          <Form.Item label="Email">
            {getFieldDecorator('email', {
              rules: [
                {
                  required: true,
                  message: 'Please input the email!',
                },
              ],
            })(<Input />)}
          </Form.Item>

          <Form.Item label="Password">
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Please input the password!',
                },
              ],
            })(<Input type="password" />)}
          </Form.Item>
        </Form>
      </Modal>
    </div>
  )
}

export default Form.create()(FormSingup)
