import { compose } from 'redux'
import { connect } from 'react-redux'

import Dashboard from '../../components/dashboard/Dashboard'
import { firestoreConnect } from 'react-redux-firebase'

const mapStateToProps = state => {
  return {
    events: state.firestore.ordered.events,
  }
}

export default compose(
  firestoreConnect(['events']),
  connect(mapStateToProps),
)(Dashboard)
