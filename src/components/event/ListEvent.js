import React, { Fragment } from 'react'
import { Col, Row } from 'antd'
import { get } from 'lodash'
import EventItem from './EventItem'

function ListEvent(props) {
  const items = get(props, 'items', [])
  return (
    <Fragment>
      <Row gutter={16}>
        {items.map(item => (
          <Col span={7} key={item.id}>
            <EventItem item={item} />
          </Col>
        ))}
      </Row>
    </Fragment>
  )
}

export default ListEvent
