import Logout from '../../components/auth/LogOut'
import { firebaseConnect } from 'react-redux-firebase'

export default firebaseConnect()(Logout)
