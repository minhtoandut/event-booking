import FormSingup from '../../components/auth/SignUp'
import { firebaseConnect } from 'react-redux-firebase'

export default firebaseConnect()(FormSingup)
