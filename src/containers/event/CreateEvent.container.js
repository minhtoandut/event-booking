import { compose, withHandlers } from 'recompose'
import CreateEvent from '../../components/event/CreateEvent'
import { firestoreConnect } from 'react-redux-firebase'

export default compose(
  firestoreConnect(['events']),
  withHandlers({
    createEvent: props => event => {
      console.log(event)
      return props.firestore.add('events', event)
    },
  }),
)(CreateEvent)
