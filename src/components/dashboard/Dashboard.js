import React, { Fragment } from 'react'
import styled from 'styled-components'
import { Button } from 'antd'
import { get } from 'lodash'

import ListEvent from '../event/ListEvent'

const UpcomingEventWrapper = styled.div`
  position: relative;
  margin-bottom: 21px;

  .btn-more {
    position: absolute;
    right: 40px;
    top: 50%;
  }
`

const HotEventWrapper = styled.div`
  position: relative;
  margin-bottom: 21px;

  .btn-more {
    position: absolute;
    right: 40px;
    top: 50%;
  }
`

function Dashboard(props) {
  const events = get(props, 'events', [])

  let hotEvents = events.filter(event => event.type === 'hot-event')
  let upcommingEvents = events.filter(event => event.type === 'upcoming-event')
  hotEvents = hotEvents.slice(0, 3)
  upcommingEvents = upcommingEvents.slice(0, 3)

  return (
    <Fragment>
      <UpcomingEventWrapper>
        <h1>Upcoming Event</h1>
        <ListEvent items={upcommingEvents} />
        <Button className="btn-more" type="primary">
          More
        </Button>
      </UpcomingEventWrapper>

      <HotEventWrapper>
        <h1>Hot Event</h1>
        <ListEvent items={hotEvents} />
        <Button className="btn-more" type="primary">
          More
        </Button>
      </HotEventWrapper>
    </Fragment>
  )
}

export default Dashboard
