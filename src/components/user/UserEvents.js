import React, { Fragment } from 'react'
import { get } from 'lodash'

import ListEvent from '../event/ListEvent'
import CreateEvent from '../../containers/event/CreateEvent.container'

function UserEvents(props) {
  const { firebase } = props
  const uid = get(firebase, 'auth.uid')
  let events = get(props, 'events', [])
  let items = events.filter(item => item.uid === uid)

  return (
    <Fragment>
      <ListEvent items={items} />
      <CreateEvent uid={uid} />
    </Fragment>
  )
}

export default UserEvents
