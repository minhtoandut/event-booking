import React, { Fragment } from 'react'
import { Layout, Menu } from 'antd'
import { Link } from 'react-router-dom'
import { get } from 'lodash'
import SignUp from '../../containers/auth/signup.container'
import SignIn from '../../containers/auth/signin.container'
import Logout from '../../containers/auth/logout.container'

const { Header } = Layout
function Navbar(props) {
  const uid = get(props, 'auth.uid', null)
  console.log(props, uid, !uid)
  return (
    <Header>
      <Menu theme="dark" mode="horizontal" style={{ lineHeight: '64px' }}>
        <Menu.Item key="1">
          <Link to="/">
            <div className="logo" />
          </Link>
        </Menu.Item>
        {!uid ? (
          <Fragment />
        ) : (
          <Menu.Item key="2">
            <Link to="your-events">Your events</Link>
          </Menu.Item>
        )}

        <Menu.Item key="3">
          <Link to="notification">Notifications</Link>
        </Menu.Item>
        {!uid ? (
          <Fragment>
            <SignIn />
            <SignUp />
          </Fragment>
        ) : (
          <Fragment>
            <Logout />
          </Fragment>
        )}
      </Menu>
    </Header>
  )
}

export default Navbar
