import React, { useState, Fragment } from 'react'
import { Button, Modal, Form, Input, DatePicker, TimePicker, Radio } from 'antd'
import moment from 'moment'

function FormCreateEvent(props) {
  const { form, createEvent, uid } = props
  const { getFieldDecorator, getFieldsValue } = form
  const [visible, setVisible] = useState(false)

  function handleCancel() {
    setVisible(false)
    form.resetFields()
  }

  function handleCreate() {
    const data = getFieldsValue()
    data.time = moment(data.time).format('hh:mm:ss')
    data.date = moment(data.date).format('MM/DD/YYYY')
    createEvent({ ...data, uid })
    setVisible(false)
    form.resetFields()
  }

  const config = {
    rules: [{ type: 'object', required: true, message: 'Please select time!' }],
  }

  return (
    <Fragment>
      <Button onClick={() => setVisible(true)}>New Event</Button>
      <Modal
        visible={visible}
        onOk={handleCreate}
        onCancel={handleCancel}
        okText="Create">
        <Form layout="vertical">
          <Form.Item label="Title">
            {getFieldDecorator('title', {
              rules: [
                {
                  required: true,
                  message: 'Please input the title of collection!',
                },
              ],
            })(<Input />)}
          </Form.Item>

          <Form.Item label="Location">
            {getFieldDecorator('location', {
              rules: [
                {
                  required: true,
                  message: 'Please input the location!',
                },
              ],
            })(<Input />)}
          </Form.Item>

          <Form.Item label="Description">
            {getFieldDecorator('description', {
              rules: [
                {
                  required: true,
                  message: 'Please input the description!',
                },
              ],
            })(<Input />)}
          </Form.Item>

          <Form.Item label="Type">
            {getFieldDecorator('type')(
              <Radio.Group>
                <Radio value="hot-event">Hot Event</Radio>
                <Radio value="upcoming-event">Upcoming Event</Radio>
              </Radio.Group>,
            )}
          </Form.Item>

          <Form.Item label="Time">
            {getFieldDecorator('time', config)(<TimePicker />)}
          </Form.Item>

          <Form.Item label="Date">
            {getFieldDecorator('date', config)(<DatePicker />)}
          </Form.Item>
        </Form>
      </Modal>
    </Fragment>
  )
}

export default Form.create()(FormCreateEvent)
