import { compose } from 'redux'
import { connect } from 'react-redux'

import UserEvent from '../../components/user/UserEvents'
import { firestoreConnect } from 'react-redux-firebase'

const mapStateToProps = state => {
  return {
    events: state.firestore.ordered.events,
    firebase: state.firebase,
  }
}

export default compose(
  firestoreConnect(['events']),
  connect(mapStateToProps),
)(UserEvent)
