import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { Layout, Breadcrumb } from 'antd'

// Implement firebase
import firebase from 'firebase'
import 'firebase/auth'
import 'firebase/firestore'
import { ReactReduxFirebaseProvider } from 'react-redux-firebase'
import { createFirestoreInstance } from 'redux-firestore'
import firebaseConfig from './configs/firebase-config'

import './index.css'
import Navbar from './containers/layout/navbar.container'
import Dashboard from './containers/dashboard/dashboard.container'
import UserEvents from './containers/user/UserEvents'
import Notification from './components/dashboard/Notification'
import NotFound from './components/auth/404'
import store from './store'

const { Content, Footer } = Layout

const rrfConfig = {
  userProfile: 'users',
  // useFirestoreForProfile: true,
}

firebase.initializeApp(firebaseConfig)

const rrfProps = {
  firebase,
  config: rrfConfig,
  dispatch: store.dispatch,
  createFirestoreInstance,
}
function App() {
  return (
    <ReactReduxFirebaseProvider {...rrfProps}>
      <BrowserRouter>
        <Layout>
          <Navbar />
          <Content style={{ padding: '0 50px' }}>
            <Breadcrumb style={{ margin: '16px 0' }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            <div
              style={{
                background: '#fff',
                padding: 24,
                minHeight: 'calc(100vh - 186px)',
              }}>
              <Switch>
                <Route path="/" exact component={Dashboard} />
                <Route path="/your-events" exact component={UserEvents} />
                <Route path="*" exact component={NotFound} />
              </Switch>
            </div>
          </Content>
          <Footer style={{ textAlign: 'center' }}>
            Ant Design ©2018 Created by Ant UED
          </Footer>
        </Layout>
      </BrowserRouter>
    </ReactReduxFirebaseProvider>
  )
}

export default App
