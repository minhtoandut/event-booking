import React from 'react'
import { Button } from 'antd'

function LogOut(props) {
  const { firebase } = props

  async function handleLogout() {
    try {
      await firebase.logout()
    } catch (err) {
      console.log(err)
    }
  }

  return (
    <Button style={{ float: 'right' }} onClick={() => handleLogout()}>
      Logout
    </Button>
  )
}

export default LogOut
